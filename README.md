# Cli Temp

## Stable releases links

Release Date: Tue Jan 12 12:46:44 MSK 2021

#### Ubuntu 20

https://gitlab.com/denis.benyaminov/cli-temp/-/blob/master/releases/stable/cyberguard-cli-ubuntu20-latest

#### Ubuntu 18

https://gitlab.com/denis.benyaminov/cli-temp/-/blob/master/releases/stable/cyberguard-cli-ubuntu18-latest

#### CentOS 7

https://gitlab.com/denis.benyaminov/cli-temp/-/blob/master/releases/stable/cyberguard-cli-centos7-latest

#### CentOS 8

https://gitlab.com/denis.benyaminov/cli-temp/-/blob/master/releases/stable/cyberguard-cli-centos8-latest
